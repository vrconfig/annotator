var imgInstance;

var loadImage = function(event) {
    	var input = event.target || window.event.srcElement,
        files = input.files;
    	
    	if (FileReader && files && files.length) {
        	var fr = new FileReader();
        	fr.onload = function () {
        	
        		var img = document.createElement("img");
				img.src = fr.result;
							
				imgInstance = new fabric.Image(img);        	
        		imgInstance.scaleToWidth(1000, false);
        	
        	  	canvas.setBackgroundImage(imgInstance, canvas.renderAll.bind(canvas));  
        	}
        	fr.readAsDataURL(files[0]);
    	}
};

var canvas = new fabric.Canvas('c', { selection: false });


var img = document.createElement("img");
img.src = 'router.jpg';
							
imgInstance = new fabric.Image(img);        	
imgInstance.scaleToWidth(1000, false);


canvas.setBackgroundImage(imgInstance, canvas.renderAll.bind(canvas));

fabric.Object.NUM_FRACTION_DIGITS = 2;
 	
var routername, devices_json;
	
var rect, isDown, origX, origY;

var edgedetection = 5; //pixels to snap


canvas.on('object:moving', function (e) {
    var obj = e.target;
    obj.setCoords(); //Sets corner position coordinates based on current angle, width and height
	

    canvas.forEachObject(function (targ) {
		
		if (canvas.getActiveObjects().length > 1){

			var returning = false;
        	obj.forEachObject(function (a) {
 				if (a == targ){
 					returning = true;
 					return;
 				}
 			});
 			if (returning){
 				return;
 			}
 		} else {
 			if(targ === obj) return;
       		
       		
 		}
 		
 		checkSnapping(obj, targ);
        checkIntersecting(obj, targ);
    });
    obj.left = Math.round(obj.left);
});



function checkSnapping(activeObject, targ){
		 // Standard snapping when within range
        if(Math.abs(activeObject.aCoords.tr.x - targ.aCoords.tl.x) < edgedetection) {
            activeObject.left = targ.left - activeObject.width*activeObject.scaleX;
        }
        if(Math.abs(activeObject.aCoords.tl.x - targ.aCoords.tr.x) < edgedetection) {
            activeObject.left = targ.left + targ.width*targ.scaleX;
        }
        if(Math.abs(activeObject.aCoords.br.y - targ.aCoords.tr.y) < edgedetection) {
            activeObject.top = targ.top - activeObject.height*activeObject.scaleY;
        }
        if(Math.abs(targ.aCoords.br.y - activeObject.aCoords.tr.y) < edgedetection) {
            activeObject.top = targ.top + targ.height*targ.scaleY;
        }

        // Snap top/left together when moving up/down or side to side if within range
        if(Math.abs(activeObject.top - targ.top) < edgedetection) {
            activeObject.top = targ.top;
        }
        if(Math.abs(activeObject.left - targ.left) < edgedetection) {
            activeObject.left = targ.left;
        }

}

function checkIntersecting(activeObject, targ){
	    if(activeObject.intersectsWithObject(targ) && targ.intersectsWithObject(activeObject)) {
            targ.set({ stroke: 'red' });
        } else {
            targ.set({ stroke: 'black' });
        }

        if(!activeObject.intersectsWithObject(targ)) {
            activeObject.set({ stroke: 'black' });
        }
}






canvas.on('mouse:down', function(o){
	if (o.e.shiftKey){
		canvas.selection = true;
 		canvas.forEachObject(function(o) {
    		o.selectable = true;
  		});
		return;
	} else {
		canvas.selection = false;
	}


    isDown = true;
    var pointer = canvas.getPointer(o.e);
    origX = pointer.x;
    origY = pointer.y;
    
	if(canvas.getActiveObjects().length == 0){
		rect = new fabric.Rect({
	        left: Math.round(origX),
	        top: origY,
	        originX: 'left',
	        originY: 'top',
	        width: pointer.x-origX,
	        height: pointer.y-origY,
	        angle: 0,
	        fill: 'rgba(0,0,200,0.3)',
	        strokeDashArray: [5, 5],
	    	stroke: 'black',
	    	strokeWidth: 2,
	        transparentCorners: false
	    });
		canvas.add(rect);
	}
    
});

canvas.on('mouse:move', function(o){
	
	if (o.e.shiftKey){
		
		return;
	}
    if (!isDown) return;
    var pointer = canvas.getPointer(o.e);
    
    if (canvas.getActiveObjects().length == 0){
   
    
    	if (origX>pointer.x) {
        	rect.set({ left: Math.abs(pointer.x) });
    	}
    	if (origY>pointer.y) {
        	rect.set({ top: Math.abs(pointer.y) });
    	}
    
    	rect.set({ width: Math.abs(origX - pointer.x) });
    	rect.set({ height: Math.abs(origY - pointer.y) });
    
    
    	canvas.renderAll();
    }
});

canvas.on('mouse:up', function(o){
	if (o.e.shiftKey){	
		return;
	}

	isDown = false;
	if(canvas.getActiveObjects().length == 0 && (rect.width > 10 || rect.height > 10)){
 
		canvas.remove(rect);
		canvas.add(rect);
		canvas.setActiveObject(rect);  
	} else if (rect.width < 10 && rect.height < 10) {
		canvas.remove(rect);
	}
	
});

canvas.on('object:modified', (e) => {
	updateFields(e.target);
});

canvas.on('selection:updated', (e) => {
	updateFields(e.target);
});

canvas.on('selection:created', (e) => {
	updateFields(e.target);
});

canvas.on('object:added', (e) => {
	updateFields(e.target);
});

fabric.Object.prototype._renderStroke = function(ctx) {
    if (!this.stroke || this.strokeWidth === 0) {
        return;
    }
    if (this.shadow && !this.shadow.affectStroke) {
        this._removeShadow(ctx);
    }
    ctx.save();
    ctx.scale(1 / this.scaleX, 1 / this.scaleY);
    this._setLineDash(ctx, this.strokeDashArray, this._renderDashedStroke);
    this._applyPatternGradientTransform(ctx, this.stroke);
    ctx.stroke();
    ctx.restore();
};
    
fabric.util.addListener(fabric.document, 'keyup', function (e) {
	if (e.keyCode === 46) {    
		var selected = canvas.getActiveObjects();
		if (selected) {
			canvas.remove(...selected);
			canvas.discardActiveObject().renderAll();
		}
	}
}); 

fabric.util.addListener(fabric.document, 'keydown', function (e) {

	 switch (e.keyCode) {
                case 38:  /* Up arrow was pressed */
                   canvas.getActiveObject().top -= 1; 
                  break;
                case 40:  /* Down arrow was pressed */
                    canvas.getActiveObject().top += 1; 
                  break;
                case 37:  /* Left arrow was pressed */
                    canvas.getActiveObject().left -= 1; 
                  break;
                case 39:  /* Right arrow was pressed */
                   canvas.getActiveObject().left += 1;                
                  break;
    }
        canvas.renderAll();
    
    canvas.forEachObject(function (targ) {
        activeObject = canvas.getActiveObject();
        activeObject.setCoords();

        if(targ === activeObject) return;
        
        checkIntersecting(activeObject, targ);

    });
    canvas.renderAll();
}); 


function duplicate() {
	canvas.getActiveObject().clone(function(cloned) {
		_clipboard = cloned;
	});

	// clone again, so you can do multiple copies.
	_clipboard.clone(function(clonedObj) {
		canvas.discardActiveObject();
		clonedObj.set({
			left: clonedObj.left + clonedObj.width*clonedObj.scaleX,
			top: clonedObj.top,
			evented: true,
		});
		if (clonedObj.type === 'activeSelection') {
			// active selection needs a reference to the canvas.
			clonedObj.canvas = canvas;
			clonedObj.forEachObject(function(obj) {
				canvas.add(obj);
			});
			// this should solve the unselectability
			clonedObj.setCoords();
		} else {
			canvas.add(clonedObj);
		}
		_clipboard.top += 10;
		_clipboard.left += 10;
		canvas.setActiveObject(clonedObj);
		canvas.requestRenderAll();
	});
}

function updateFields(o){
	o.left = Math.round(o.left);
	o.top = Math.round(o.top);
	o.angle = Math.round(o.angle);
	o.height = Math.round(o.height);
	o.width = Math.round(o.width);
 	
	o.aCoords.tl.x=Math.round(o.aCoords.tl.x);
	o.aCoords.tl.y=Math.round(o.aCoords.tl.y);
	o.aCoords.tr.x=Math.round(o.aCoords.tr.x);
	o.aCoords.tr.y=Math.round(o.aCoords.tr.y);
	o.aCoords.br.x=Math.round(o.aCoords.br.x);
	o.aCoords.br.y=Math.round(o.aCoords.br.y);
	o.aCoords.bl.x=Math.round(o.aCoords.bl.x);
	o.aCoords.bl.y=Math.round(o.aCoords.bl.y);
 		
	$('[name=optradio]').prop('checked',false);
	$('[name=optradio][value='+o.nameTag+']').prop('checked',true);
	$('#json-output').val(createJSON()); 
}




   
    function download(filename, text) {
    	var pom = document.createElement('a');
   		pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    	pom.setAttribute('download', filename);

    	if (document.createEvent) {
        	var event = document.createEvent('MouseEvents');
        	event.initEvent('click', true, true);
        	pom.dispatchEvent(event);
   		}
    	else {
        	pom.click();
    	}
	}
	
	function downloadImage(filename, text) {
		var pom = document.createElement('a');
		pom.setAttribute('href', text);
    	pom.setAttribute('download', filename);

    	if (document.createEvent) {
        	var event = document.createEvent('MouseEvents');
        	event.initEvent('click', true, true);
        	pom.dispatchEvent(event);
   		}
    	else {
        	pom.click();
    	}
	}
	
	function saveImage() {
    	var dataurl = imgInstance.toDataURL({
        	format: 'jpeg',
        	quality: 1
    	});
    	downloadImage(routername+".jpg",dataurl);
	}
    
    function save(){
    	var str = createJSON(); 
   
		var filename = routername + ".json";
		download(filename, str);   
    }
    
    function createJSON(){
    	var str = [];  
   
    	for (i of canvas.getObjects()) {

    		var element = [];
    		element.push(i.aCoords);
    		element.push(i.angle);
    		element.push(i.height*i.scaleY);
    		element.push(i.width*i.scaleX);
    		element.push(i.nameTag);
    	
    		str.push(element);
		}
		str = JSON.stringify(str);

		return str;
    }
    
    function createNewRect(){
    	rect1 = new fabric.Rect({
    	    left: 10,
    	    top: 10,
    	    originX: 'left',
    	    originY: 'top',
    	    width: 10,
    	    height: 10,
    	    angle: 0,
    	    fill: 'rgba(0,0,200,0.3)',
    	    strokeDashArray: [5, 5],
    		stroke: 'black',
    		strokeWidth: 2,
    	    transparentCorners: false
    	});
    	
    	return rect1;
    }
    
    function setNameTag(myRadio){
    	if (canvas.getActiveObjects().length == 0) {
    		var objects = [];
    		for (i of canvas.getObjects()) {
    			if (i.nameTag == myRadio.value){
    				objects.push(i);
    			}
    		}
      		var group = new fabric.ActiveSelection(objects);
				
  			canvas.setActiveObject(group);
			myRadio.checked = true; // check manually again since "setActiveObject()" seems to deselect everything
    				
    	} else {
    		var o = canvas.getActiveObject();
    		o.nameTag = myRadio.value;
    	}  
    	canvas.requestRenderAll();  
    }
    
	function setRouterName(){
		
		routername = devices_json[$('#routername').find(":selected").text()];       
    }

    function load(text){
  
    	var a = JSON.parse(text);
    	
    	for (i of a) {
    		rect2 = createNewRect();
    		
    		rect2.angle = i[1];
   	 		rect2.left = i[0].tl.x;
    		rect2.top = i[0].tl.y;
    		rect2.height = i[2];
    		rect2.width = i[3];
    		rect2.nameTag = i[4];
    		
    		canvas.add(rect2);
		} 
    }
    
    var openFile = function(event) {
    	var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
        	var text = reader.result;
        	load(text);
        	console.log(reader.result.substring(0, 200));
        };
        reader.readAsText(input.files[0]);
      };
      
	function loadLocalJSON(text){
		var a = JSON.parse(text);
		devices_json = a;
		routerselect = document.getElementById('routername');

    	for (var i in a) {	
    		routerselect.options[routerselect.options.length] = new Option(i, i);
    	}	
    }
      
      function readJSON(path) {
			var xhr = new XMLHttpRequest();
        	xhr.open('GET', path, true);
        	xhr.responseType = 'blob';
        	xhr.onload = function(e) { 
          		if (this.status == 200) {
            		var file = new File([this.response], 'temp');
              		var fileReader = new FileReader();
              		fileReader.addEventListener('load', function(){
              			loadLocalJSON(fileReader.result);  		
              		});
              		fileReader.readAsText(file);
          		} 
        	}
        	xhr.send();
    	}
